# Bit Bucket Repositories Backup

Backup your bitbucket repositories by git pulling all
branches in Bash scripts.

## Installation

Install json parser:
```
  jq
```

Create an app_password with read access to repositories:
```
  https://support.atlassian.com/bitbucket-cloud/docs/create-an-app-password/
```

Set your user_name and app_password in:
```
  get_repositories_info.sh
```

Upload a public ssh key of your user to:
```
  https://bitbucket.org/account/settings/ssh-keys/
```

## Run

To clone and pull working copies of all your repositories in the
repositories subdir, run:
```
  ./bitbucketbackup.sh
```
