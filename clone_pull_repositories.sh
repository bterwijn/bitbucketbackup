#!/bin/bash
#

# upload your public key to https://bitbucket.org/account/settings/ssh-keys/

mkdir -p repositories
cd repositories

filename="../all_repos.json"
while IFS= read -r line
do
    repo=${line//\"/}
    echo "repo: $repo"
    dir_name=$(basename "$repo")
    dir_name=${dir_name%%.*}
    echo "dir_name: $dir_name"
    if [[ ! -d "$dir_name" ]]
    then
        echo "### git clone $repo"
        git clone $repo
    fi
    echo "### cd $dir_name"
    cd $dir_name
    echo "### git branch -r"
    git branch -r | grep -v '\->' | while read remote; do
        IFS='/' read origin branch <<< "${remote}"
        #echo "local branch '$branch' tracks remote branch '$remote'"
        echo "### git branch --track $branch $remote"
        git branch --track $branch $remote 2>/dev/null
    done
    echo "### git pull --no-rebase --all"
    git pull --no-rebase --all
done < "$filename"
cd ..
