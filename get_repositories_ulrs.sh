#!/bin/bash
#

USERNAME=''
APP_PASSWORD=''

next_url=$(echo "https://api.bitbucket.org/2.0/repositories/bterwijn")
count=1
do=true
while $do
do
    echo "reading url: $next_url"

    filename=$(echo "./page$count.json")
    curl --request GET \
         --url $next_url \
         --header 'Accept: application/json' \
         --user "$USERNAME:$APP_PASSWORD" > $filename
    echo "save file: $filename"

    repos=$(echo "./repos$count.json")
    #jq '.values[] | .links.html.href' $filename > $repos
    jq '.values[] | .links.clone[] | if (.name|contains("ssh")) then .href else empty end' $filename > $repos
    echo "save file: $repos"
    
    next_url=$(jq .next $filename)
    next_url=${next_url//\"/}
    [[ "$next_url" == null ]] && do=false
    count=$((count+1))
done
cat repos*.json > all_repos.json
rm -f repos*.json page*.json
